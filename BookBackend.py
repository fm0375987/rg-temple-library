
import json
import os


def read_json(filename: str, wd: str = "DB") -> list or dict:

    """
    :param filename: the file to be read (don't include the .json)
    :return: the objets in the file as python objects

    Reads a wd/.json file and returns the content as a python object
    """

    f = open(f'{wd}/{filename}', 'r')
    data = f.read() # FIXME: Fails to decode byte 0xb0 in position 37
    f.close()

    return json.loads(data)


def write_json(filename: str, data):

    """
    :param filename: the name of the file (don't include .json)
    :param data: the data to be written

    Writes a .json file from a python object
    """

    f = open(f'{filename}.json', 'w')
    f.write(json.dumps(data))
    f.close()


def delete_char(char: str, string: str) -> str:

    """
    :param char: the character to delete
    :param string: the string from which the character should be deleted

    :return: a string, where the specified character has been deleted
    """

    edit = string.replace(char, '')

    return edit


def search_book(type: str, input: str, lang=['all']) -> list:

    """
    :param lang: the languag to search by, by default it is all
    :param input: the actual string to search by
    :param type: search by title or author

    Searches for a specific .json book file by name, author or language,
    by default the language is 'all'

    :return: all filenames which match the inputs
    """

    result = []

    for filepath in os.listdir('DB'):

        if (type == 'name') or (type == 'Name') or (type == 'title') or (type == 'Title'):

            if delete_char(' ', input).lower() in delete_char(' ', filepath.split('.')[0]).lower():

                if ('all' in lang) or ('All' in lang):
                    result.append(filepath)

                elif (read_json(filepath)['lang'] in lang) or (read_json(filepath)['lang'].lower() in lang):
                    result.append(filepath)

        if (type == 'author') or (type == 'Author'):

            if delete_char(' ', input).lower() in delete_char(' ', read_json(filepath)['author']).lower():

                if ('all' in lang) or ('All' in lang):
                    result.append(filepath)

                elif (read_json(filepath)['lang'] in lang) or (read_json(filepath)['lang'].lower() in lang):
                    result.append(filepath)

    return result


def add_book(name: str, author: str, lang: str, count: int):

    """
    :param name: title of the book
    :param author: author of the book
    :param lang: language of the book
    :param count: how many books

    Adds a new .json file, if one exists, it will not replace it
    """

    search_name = delete_char(' ', name)
    files = os.listdir()
    search_files = []

    for file in files:
        search_files.append(delete_char(' ', file.split('.')[0]))

    if search_name in search_files:

        file = files[search_files.index(search_name)]

        if lang == read_json(file.split('.')[0])['lang']:

            if author in read_json(file.split('.')[0])['author']:
                data = {'author': author, 'lang': lang, 'count': int(read_json(file.split('.')[0])['count']) + int(count)}
                write_json(file.split('.')[0], data)
            else:
                data = {'author': author, 'lang': lang, 'count': int(count)}
                write_json(f'{file.split(".")[0]}_{author}', data)

        elif author in read_json(file.split('.')[0])['author']:
            data = {'author': author, 'lang': lang, 'count': int(count)}
            write_json(f'{file.split(".")[0]}_{lang.split()[:3]}', data)

        else:
            data = {'author': author, 'lang': lang, 'count': int(count)}
            write_json(f'{file.split(".")[0]}_{lang.split()[:3][0]}_{author}', data)
    else:
        data = {'author': author, 'lang': lang, 'count': int(count)}
        write_json(name, data)
