import sqlite3
import json
import os, sys
from Book import Book

def import_json_files_into_db(json_dir, db_file):
    # TODO:  Check if json_dir and db_file exists

    # Connect to DB and create cursor
    conn = sqlite3.connect(db_file)
    c = conn.cursor()

    # Create Books table, if it does not already exists
    c.execute('''CREATE TABLE IF NOT EXISTS Books ([name] text, [author] text, [language] text, [count] int, [loan] text)''')

    # For every json file in json_dir, get fields
    for json_file in os.listdir(json_dir):
        if json_file.endswith(".json") and not json_file.startswith("._"):
            full_path = os.path.join(json_dir, json_file)
            record = extract_data(full_path)
            if record is None:
                insert_string = '''INSERT INTO Books (name, author, language, count) VALUES ("{}", "", "", "")'''.format(json_file)
            else:
                insert_string = '''INSERT INTO Books (name, author, language, count) VALUES ("{}", "{}", "{}", "{}")'''.format(json_file, record["author"], record["lang"], record["count"])
            c.execute(insert_string)

    conn.commit()
    conn.close()


# Given a json_file as input, this function returns a dictionary of extracted fields
def extract_data(json_file) -> bool:
    f = open(json_file)
    data = f.read()
    #data.replace("\\n", "")
    try:
        extracted_data = json.JSONDecoder().decode(data)
        return extracted_data
    except:
        print("Error decoding: ", sys.exc_info()[0])
        return False
    finally:
        f.close()
    return True


# Search for a book by name
def search_name(book):
    pass

# Search for a book by author
def search_author(book):
    pass

# Add a new book
def add(book):
    pass


# Delete a book
def delete(book: str) -> bool:
    """
    Instead of deleting the file itself,
    it is a better idea to move it into
    a 'Bought' directory.
    """

    if not os.path.exists(book):
        print("Book Doesn't exist")
        return False
    else:
        if not extract_data(book):
            print("Failed to extract data from " + book)

    if not os.path.exists("Bought"):
        os.makedirs("Bought")

    os.rename("./" + book, "./Bought/" + book)
    return True

# Update a book
def update(book):
    pass

import_json_files_into_db("./DB", "./BooksDB.db")
