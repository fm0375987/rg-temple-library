
import json

def write_json(filename, data):

    json_data = json.dumps(data)
    f = open(f'{filename}.json', 'w')
    f.write(json_data)
    f.close

def read_json(filename):

    f = open(f'{filename}.json', 'r')
    data = json.loads(f.read())
    return data


