
import funcs
import os

while True:

    book = input('Write all the book data here, seperate by ,:\n')
    book = book.split(',')
    
    try:
        for files in os.listdir("."):
            if files == book[0] + ".json":
                _ = funcs.read_json(book[0])
                if book[1] == _["author"] and book[2] == _["lang"]:
                    print("Same author")
                    book[3] = str(int(book[3]) + _["count"])
                    
        book_data = {'author': book[1], 'lang': book[2], 'count': int(book[3])}
        funcs.write_json(book[0], book_data)
        
    except IndexError:
        print('try again')


