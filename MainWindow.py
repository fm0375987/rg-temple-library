from PySide6.QtWidgets import QDialog, QWidget, QPushButton, QMainWindow, QGridLayout, QVBoxLayout, QHBoxLayout, QLabel

from SearchFrontend import *

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("RG Temple Library")

        self.layout = QGridLayout()
        self.layout.addWidget(QLabel("Welcome to RG Temple Library Application", self))
        self.layout.addWidget(QPushButton("Search Book", self))
        self.layout.addWidget(QPushButton("Buy Book", self))
        self.layout.addWidget(QPushButton("Loan Book", self))
        self.layout.addWidget(QPushButton("Add Book", self))
        self.setLayout(self.layout)

