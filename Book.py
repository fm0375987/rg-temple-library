# Class defining Book interface.
class Book:

    def __init__(self, name, author, lang, count):
        self.name = name
        self.author = author
        self.language = lang
        self.count = count

