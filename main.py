#!/usr/bin/python3

from PySide6.QtWidgets import QApplication

import sys

from MainWindow import MainWindow

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWidget = MainWindow()
    mainWidget.show()
    sys.exit(app.exec())
